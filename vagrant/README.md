# Introdução

Assumindo que você tenha o vagrant 2.0+ instalado com virtualbox você deve ser capaz de lançar um cluster docker-swarm de 3 nós simplesmente executando o `vagrant` up. Isso vai criar 3 VMs e instalar docker-swarm nelas. Uma vez que eles estejam completos, você pode se conectar a qualquer um deles rodando o `vagrant ssh swarm-[1..3]` do vagrant.

## Requerimentos

- vagrant >= 2.0
- virtualbox
