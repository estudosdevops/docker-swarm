# -*- mode: ruby -*-
# vi: set ft=ruby :

# For help on using docker-swarm with vagrant, check out vagrant/README.md

require 'fileutils'

Vagrant.require_version ">= 2.0.0"

CONFIG = File.join(File.dirname(__FILE__), "vagrant/config.rb")

SUPPORTED_OS = {
  "ubuntu1804"          => {box: "ubuntu/bionic64", user: "vagrant"},
  "centos"              => {box: "centos/7",           user: "vagrant"},
  "centos-bento"        => {box: "bento/centos-7.6",   user: "vagrant"},
  "fedora"              => {box: "fedora/28-cloud-base", user: "vagrant"},
}

# Defaults for config optons defined in CONFIG
$num_instances = 3
$instance_name_prefix = "swarm"
$vm_gui = false
$vm_memory = 2048
$vm_cpus = 1
$shared_folders = {}
$forwarded_ports = {}
$subnet = "172.17.8"
$os = "ubuntu1804"

# Setting multi_networking to true will install Multus: https://github.com/intel/multus-cni
$multi_networking = false

# The first node is a manager
$swarm_manager_instances = $num_instances == 1 ? $num_instances : ($num_instances - 2)

# The last two nodes are workers
$swarm_worker_instances = $num_instances

# Settings for ansible
$playbook = "cluster.yml"
host_vars = {}

if File.exist?(CONFIG)
    require CONFIG
end

$box = SUPPORTED_OS[$os][:box]
# if $inventory is not set, try to use example
$inventory = "inventory/sample" if ! $inventory
$inventory = File.absolute_path($inventory, File.dirname(__FILE__))

# if $inventory has a hosts.ini file use it, otherwise copy over
# vars etc to where vagrant expects dynamic inventory to be
if ! File.exist?(File.join(File.dirname($inventory), "hosts.ini"))
  $vagrant_ansible = File.join(File.dirname(__FILE__), ".vagrant", "provisioners", "ansible")
  FileUtils.mkdir_p($vagrant_ansible) if ! File.exist?($vagrant_ansible)
  if ! File.exist?(File.join($vagrant_ansible,"inventory"))
    FileUtils.ln_s($inventory, File.join($vagrant_ansible,"inventory"))
  end
end

if Vagrant.has_plugin?("vagrant-proxyconf")
  $no_proxy = ENV['NO_PROXY'] || ENV['no_proxy'] || "127.0.0.1,localhost"
  (1..$num_instances).each do |i|
      $no_proxy += ",#{subnet}.#{i+100}"
  end
end

Vagrant.configure("2") do |config|
  config.vm.box = $box
  if SUPPORTED_OS[$os].has_key? :box_url
    config.vm.box = SUPPORTED_OS[$os][:box_url]
  end
  config.ssh.username = SUPPORTED_OS[$os][:user]

  # plugin conflict
  if Vagrant.has_plugin?("vagrant-vbguest") then
    config.vbguest.auto_update = false
  end

  # always use Vagrants insecure key
  config.ssh.insert_key = false

  (1..$num_instances).each do |i|
    config.vm.define vm_name = "%s-%01d" % [$instance_name_prefix, i] do |node|
      node.vm.hostname = vm_name

      if Vagrant.has_plugin?("vagrant-proxyconf")
        node.proxy.http      = ENV['HTTP_PROXY'] || ""
        node.proxy.https     = ENV['HTTPS_PROXY'] || ENV['https_proxy'] ||  ""
        node.proxy.no_proxy  = $no_proxy
      end

      node.vm.provider :virtualbox do |vb|
        vb.memory = $vm_memory
        vb.cpus = $vm_cpus
        vb.gui = $vm_gui
        vb.linked_clone = true
        vb.customize ["modifyvm", :id, "--vram", "8"] # ubuntu defaults to 256 MB which is a waste of precious RAM
      end

    ip = "#{$subnet}.#{i+100}"
    node.vm.network :private_network, ip: ip

    # Bootstrap for each vm
    config.vm.provision :shell, :path => "vagrant/bootstrap.sh"

    host_vars[vm_name] = {
      "ip": ip,
      "swarm_network_plugin_multus": $multi_networking,
      "ansible_ssh_user": SUPPORTED_OS[$os][:user]
    }

    # Only execute the Ansible provisioner once, when all the machines are up and ready
    if i == $num_instances
      node.vm.provision "ansible" do |ansible|
        ansible.playbook = $playbook
        $ansible_inventory_path = File.join( $inventory, "hosts.ini")
        if File.exist?($ansible_inventory_path)
          ansible.inventory_path = $ansible_inventory_path
        end
        ansible.become = true
        ansible.limit = "all,localhost"
        ansible.host_key_checking = false
        ansible.raw_arguments = ["--forks=#{$num_instances}", "--flush-cache", "-e ansible_become_pass=vagrant"]
        ansible.host_vars = host_vars
        ansible.groups = {
          "swarm-manager" => ["#{$instance_name_prefix}-[1:#{$swarm_manager_instances}]"],
          "swarm-worker" => ["#{$instance_name_prefix}-[2:#{$swarm_worker_instances}]"],
          "swarm-cluster:children" => ["swarm-manager", "swarm-worker"],
        }
      end
    end

   end
 end
end
